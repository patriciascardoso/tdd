package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDoisNumeros(){
        int resultado = Calculadora.somar(2,2);
        Assertions.assertEquals(4,resultado);
    }


    @Test
    public void testarDivisaoDoisNumerosInteiros(){
        double resultado = Calculadora.dividir(6,2);
        Assertions.assertEquals(3,resultado);
    }

    @Test
    public void testarDivisaoDoisNumerosInteirosResultadoFlutuante(){
        double resultado = Calculadora.dividir(10,3);
        Assertions.assertEquals(3.33,resultado);
    }

    @Test
    public void testarDivisaoDoisNumerosFlutuantes(){
        double resultado = Calculadora.dividir(5.5,2.5);
        Assertions.assertEquals(2.2,resultado);
    }

    @Test
    public void testarMultiplicacaoDoisNumerosInteiros(){
        int resultado = Calculadora.multiplicar(5,5);
        Assertions.assertEquals(25,resultado);
    }

    @Test
    public void testarMultiplicacaoDoisNumerosFlutuantes(){
        double resultado = Calculadora.multiplicar(5.5,5);
        Assertions.assertEquals(27.5,resultado);
    }


}
