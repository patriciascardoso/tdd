package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {


    private Cliente cliente;
    private Conta conta;

    @BeforeEach
    public void setUp(){
        this.cliente = new Cliente ("Vinicius");
        this.conta = new Conta (cliente, 100.00);
    }

    @Test
    public void testarDepositoConta(){
        double valorDeposito = 400.00;
        conta.depositar(valorDeposito);
        Assertions.assertEquals(500.00,conta.getSaldo());
    }

    @Test
    public void sacarDeConta(){
        double valorDeposito = 50.00;
        conta.sacar(valorDeposito);
        Assertions.assertEquals(50.00,conta.getSaldo());
    }

    @Test
    public void testarSaqueSemSaldoNaConta(){
        double valorDeSaque = 400.00;
        Assertions.assertThrows(RuntimeException.class,() -> {conta.sacar(valorDeSaque);});
    }

}
