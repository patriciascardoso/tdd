package com.br.tdd;

public class Calculadora {

    public static int somar(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static double dividir (double primeiroNumero, double segundoNumero) {
        double resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public static double dividir (Integer primeiroNumero, Integer segundoNumero) {
        double resultado = primeiroNumero.doubleValue() / segundoNumero.doubleValue();
        return resultado;
    }

    public static int multiplicar (int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }


    public static double multiplicar (double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

}
